# **Git**

## **What Is Git**

Git is a version control system that you download onto your computer. It is essential that you use Git if you want to collaborate with other developers on a coding project or work on your own project.

In order to check if you already have Git installed on your computer you can type the command git --version in the terminal.

---

## **Why Use It**

### **Feature Branch Workflow**
  
One of the biggest advantages of Git is its branching capabilities. Unlike centralized version control systems, Git branches are cheap and easy to merge. This facilitates the feature branch workflow popular with many Git users.
 ![Pull Requests](/assets/images/Feature_Branch_Workflow.svg)

### **Distributed Development**

In SVN, each developer gets a working copy that points back to a single central repository. Git, however, is a distributed version control system. Instead of a working copy, each developer gets their own local repository, complete with a full history of commits.

 ![Pull Requests](/assets/images/Feature_Branch_Workflow.svg)


Having a full local history makes Git fast, since it means you don’t need a network connection to create commits, inspect previous versions of a file, or perform diffs between commits.

Distributed development also makes it easier to scale your engineering team. If someone breaks the production branch in SVN, other developers can’t check in their changes until it’s fixed. With Git, this kind of blocking doesn’t exist. Everybody can continue going about their business in their own local repositories.

And, similar to feature branches, distributed development creates a more reliable environment. Even if a developer obliterates their own repository, they can simply clone someone else’s and start anew.

### **Pull Requests**

Many source code management tools such as Gitlab and Github enhance core Git functionality with pull requests. A pull request is a way to ask another developer to merge one of your branches into their repository. This not only makes it easier for project leads to keep track of changes, but also lets developers initiate discussions around their work before integrating it with the rest of the codebase.

 ![Pull Requests](/assets/images/Pull_Requests.svg)

 ---


 ## **SSH KEY**

 ### **What is an SSH KEY?**

 An SSH key is an access credential for the SSH (secure shell) network protocol. This authenticated and encrypted secure network protocol is used for remote communication between machines on an unsecured open network. SSH is used for remote file transfer, network management, and remote operating system access. The SSH acronym is also used to describe a set of tools used to interact with the SSH protocol.

 ### **How to Create an SSH Key**

 If you don't already have an SSH key, you must generate a new SSH key to use for authentication. If you're unsure whether you already have an SSH key, you can check for existing keys. For more information, see "Checking for existing SSH keys."

#### **Creating the Key Pair**
 - The first step is to create a key pair on the client machine. This will likely be your local computer. Type the following command into your local command line:

```
ssh-keygen -t ed25519
```

**Output**

```
Generating public/private ed25519 key pair.
```

#### **Specifying Where to Save the Keys**

The first prompt from the ssh-keygen command will ask you where to save the keys:

**Output**

```
Enter file in which to save the key (/home/sammy/.ssh/id_ed25519):
```

#### **Creating a Passphrase**

The second and final prompt from ssh-keygen will ask you to enter a passphrase:

**Output**

```
Enter passphrase (empty for no passphrase):
```

It’s up to you whether you want to use a passphrase, but it is strongly encouraged: the security of a key pair, no matter the encryption scheme, still depends on the fact that it is not accessible to anyone else.

#### **Recap**

To recap, the entire key generation process looks like this:
```
ssh-keygen -t ed25519
```

**Output**
```
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/sammy/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/sammy/.ssh/id_ed25519
Your public key has been saved in /home/sammy/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:EGx5HEXz7EqKigIxHHWKpCZItSj1Dy9Dqc5cYae+1zc sammy@hostname
The key's randomart image is:
+--[ED25519 256]--+
| o+o o.o.++      |
|=oo.+.+.o  +     |
|*+.oB.o.    o    |
|*. + B .   .     |
| o. = o S . .    |
|.+ o o . o .     |
|. + . ... .      |
|.  . o. . E      |
| .. o.   . .     |
+----[SHA256]-----+
```

The public key is now located in ``/home/sammy/.ssh/id_ed25519.pub``

#### **Copying the Public Key to Your Server**

Once the key pair is generated, it’s time to place the public key on the server that you want to connect to.

You can copy the public key into the server’s authorized_keys file with the ssh-copy-id command. Make sure to replace the example username and address:

```
ssh-copy-id sammy@your_server_address
```

or

```
cat ~/.ssh/id_ed25519.pub | clip
```

Once the command completes, you will be able to log into the server via SSH without being prompted for a password. However, if you set a passphrase when creating your SSH key, you will be asked to enter the passphrase at that time. This is your local ssh client asking you to decrypt the private key, it is not the remote server asking for a password.

---

## **GitHub - Git Cheat Sheet**
### **Starting A project**

```
$ git init [project name]
```
Create a new local repository. If [project name] is provided, Git will
create a new directory name [project name] and will initialize a
repository inside it. If [project name]is not provided,then a new repository is initialized in the current directory.

```
$ git clone [project url]
```

Downloads a project with the entire history from the remote repository.

### **Day-to-Day work**

```
$ git status
```
Displays the status of your working directory. Options include new,
staged, and modified files. It will retrieve branch name, current commit
identifier, and changes pending commit.
```
$ git add [file]
```
Add a file to the staging area. Use in place of the full file path to add all
changed files from the current directory down into the directory tree.

```
$ git reset [file]
```
Revert your repository to a previous known working state.
```
$ git commit
```
Create a new commit from changes added to the staging area.
The commit must have a message!

```
$ git rm [file]
```
Remove file from working directory and staging area.
```
$ git stash
```
Put current changes in your working directory into stash for later use.

```
$ git stash pop
```
Apply stored stash content into working directory, and clear stash.
```
$ git stash drop
```
Delete a specific stash from all your previous stashes.

### **Git branching model**

```
$ git branch [-a]
```

List all local branches in repository. With -a: show all branches
(with remote).

Create new branch, referencing the current HEAD.

```
$ git checkout [-b][branch_name]
```
Switch working directory to the specified branch. With -b: Git will
create the specified branch if it does not exist.

```
$ git merge [from name]
```
Join specified [from name] branch into your current branch (the one
you are on currently).

```
$ git branch -d [name]
```
Remove selected branch, if it is already merged into any other.
-D instead of -d forces deletion.

### **Review your work**
```
$ git log [-n count]
```
List commit history of current branch. -n count limits list to last n
commits.
```
$ git log --oneline --graph --decorate
```
An overview with reference labels and history graph. One commit
per line.
```
$ git log ref..
```
List commits that are present on the current branch and not merged
into ref. A ref can be a branch name or a tag name.
```
$ git log ..ref
```
List commit that are present on ref and not merged into current
branch.
```
$ git reflog
```
List operations (e.g. checkouts or commits) made on local repository.

### **Reverting changes**
```
$ git reset [--hard] [target reference]
```
Switches the current branch to the target reference, leaving
a difference as an uncommitted change. When --hard is used,
all changes are discarded.
```
$ git revert [commit sha]
```
Create a new commit, reverting changes from the specified commit.
It generates an inversion of changes.

### **Synchronizing repositories**

Fetch changes from the remote, but not update tracking branches.
```

$ git fetch --prune [remote]
```
Delete remote Refs that were removed from the remote repository.
```
$ git pull [remote]
```
Fetch changes from the remote and merge current branch with its
upstream.

```
$ git push [--tags] [remote]
```
Push local changes to the remote. Use --tags to push tags.

```
$ git push -u [remote] [branch]
```
Push local branch to remote repository. Set its copy as an upstream.

---

|       |                                                        |  
|-------|--------------------------------------------------------|
|Commit | An Object                                              |
|Branch | A reference to a commit,can have a tracked upstream    |   
|Tag    | A reference(standard) or an object(annotated)          |   
|Head   | A place where your working directory is now            |   

### **Ignoring Files**

**.gitignore**
Git sees every file in your working copy as one of three things:

- tracked - a file which has been previously staged or committed;
- untracked - a file which has not been staged or committed; or
- ignored - a file which Git has been explicitly told to ignore.

Ignored files are usually build artifacts and machine generated files that can be derived from your repository source or should otherwise not be committed.

Ignored files are tracked in a special file named .gitignore that is checked in at the root of your repository. There is no explicit git ignore command: instead the ``` .gitignore ``` file must be edited and committed by hand when you have new files that you wish to ignore. ```.gitignore``` files contain patterns that are matched against file names in your repository to determine whether or not they should be ignored.

# Resume 

 ![Resume](/assets/images/Resume.svg)