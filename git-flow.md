# Contribute like a Pro

Building a software in a team requires a clear understanding and proper management of the codebase. If the team members only know the "pull-push", you'll soon find yourself in the trap of resolving merge conflicts!

Developing while shipping the software may become tricky if the team is not familiar with version control. Git-flow plays a great role in agile development. I insist you to read every single word in this article, don't skim. Let's start exploring git-branching model.
 
## **Git-branching model**

Implementing the git-flow, your repo should contain two main branches:
- master (or main)
- develop
- 
Master branch is the main branch which is automatically created once you initialize a repo in a git. It is the origin of the repo and pre-exist. You'll find it written ```origin/master```. This is the branch which must contain "clean codes", the codebase which is ready for deployment. You **should not** contribute or push codes direct to this repo! I'll tell you where to push your codes.

Develop branch is the branch which you should create. I advice you, to do this even if you're building alone. This is a branch which contains active codebase, the project at development state. Whether the feature is working or buggy, this is the right place for it. This is how you start:

Create a develop branch and switch it on

```
$ git checkout -b develop
```
Pull the codebase from master branch into your current branch(develop)
```
$ git pull origin master
```

## **Other branches**

Apart from the main two branches, there are three other branches. All these branches should be created within develop branch. These branches are:

- Features branch
- Hotfix branch
- Release branch

This is how you create them:

Created as a sub-branch of develop branch (replace feature-backup with a branch name)
```
$ git checkout -b feature-backup develop
```
Or a hotfix branch
```
$ git checkout -b hotfix-443 develop
```

## **Naming convention and merging**
Naming these branches follows the standard way. Such as ```feature-*```,```hotfix-*``` and ```release-*```. So for example; ```feature-login```, or ```release-1.0.1```

After all your implementation in the feature branch (or any other branch apart from main branches) you should merge it back to develop branch then later to master branch (during production). This is how you do it:

Switch to develop branch
```
$ git checkout develop
```
Merge the feature branch
```
$ git merge --no-ff feature-backup
```
Merging with flag --no-ff creates a new commit record. This helps to store historical information of the feature branch even after the merge. Thus, makes it easy to roll back in a particular feature just from the develop branch (even after merging).

 ![Resume](/assets/images/merge-no-off.svg)
## Develop and Master Branches
Instead of a single ```master``` branch, this workflow uses two branches to record the history of the project. The master branch stores the official
release history, and the ```develop``` branch serves as an integration branch for features. It's also convenient to tag all commits in the ```master``` branch with a version number.

The first step is to complement the default ```master``` with a develop branch. A simple way to do this is for one developer to create an empty ```develop``` branch locally and push it to the server:
```
git branch develop
```
```
git push -u origin develop
```
This branch will contain the complete history of the project, whereas ```master``` will contain an abridged version. Other developers should now
clone the central repository and create a tracking branch for develop.

## **Feature Branches**
Each new feature should reside in its own branch, which can be pushed to the central repository for backup/collaboration. But, instead of
branching off of ```master```, ```feature``` branches use ```develop``` as their parent branch. When a feature is complete, it gets merged back into ```develop```.
Features should never interact directly with ```master```. 

